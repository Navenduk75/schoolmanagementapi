﻿namespace SchoolManagementAPI.ViewModels
{
    public class BlockModel
    {
        public string Name { get; set; }
        public int DistrictId { get; set; }
    }
}