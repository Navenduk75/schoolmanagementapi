﻿namespace SchoolManagementAPI.ViewModels
{
    public class BlockVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string District { get; set; }
    }
}