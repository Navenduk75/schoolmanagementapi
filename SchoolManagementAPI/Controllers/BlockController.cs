﻿using SchoolManagementAPI.DAL;
using SchoolManagementAPI.DAL.DataModels;
using SchoolManagementAPI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SchoolManagementAPI.Controllers
{
    public class BlockController : ApiController
    {
        // GET: api/Block
        public List<BlockVM> Get()
        {
            List<BlockVM> blocks = new List<BlockVM>();
            using (var context = new SchoolDbContext())
            {
                blocks = context.Blocks.Include("District")
                    .Select(b => new BlockVM { Id = b.BId, Name = b.BName, District = b.District.DName }).ToList();
            }
            return blocks;
        }

        // GET: api/Block/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Block
        public void Post([FromBody]BlockModel blockModel)
        {
            using (var context = new SchoolDbContext())
            {
                District district = context.Districts.First(d => d.DId == blockModel.DistrictId);

                Block b = new Block();
                b.BName = blockModel.Name;
                b.District = district; //.DistrictId = blockModel.DistrictId;

                context.Blocks.Add(b);

                context.SaveChanges();
            }
        }

        // PUT: api/Block/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Block/5
        public void Delete(int id)
        {
        }
    }
}
