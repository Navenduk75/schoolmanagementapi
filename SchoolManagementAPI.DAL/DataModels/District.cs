﻿using System.Collections.Generic;

namespace SchoolManagementAPI.DAL.DataModels
{
    public class District
    {
        public int DId { get; set; }
        public string DName { get; set; }

        public List<Block> Blocks { get; set; }
    }
}
