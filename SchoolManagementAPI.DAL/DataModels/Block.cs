﻿namespace SchoolManagementAPI.DAL.DataModels
{
    public class Block
    {
        public int BId { get; set; }
        public string BName { get; set; }

        public int DistrictId { get; set; }
        public District District { get; set; }
    }
}
