﻿using SchoolManagementAPI.DAL.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolManagementAPI.DAL
{
    public class SchoolDbContext : DbContext
    {
        public SchoolDbContext() : base("name=SchoolDbConnection")
        {
            // Database.SetInitializer(new CreateDatabaseIfNotExists<SchoolDbContext>());
            // Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SchoolDbContext>());
            Database.SetInitializer<SchoolDbContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<District> Districts { get; set; }
        public DbSet<Block> Blocks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.HasDefaultSchema("dbo");

            modelBuilder.Entity<District>().ToTable("Districts");
            modelBuilder.Entity<District>().HasKey(t => t.DId);

            modelBuilder.Entity<District>().Property(x => x.DId).HasColumnName(@"Id").IsRequired()
                .HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<District>().Property(x => x.DName).HasColumnName(@"Name").IsOptional()
                .HasColumnType("nvarchar").HasMaxLength(50);


            modelBuilder.Entity<Block>().ToTable("Blocks");
            modelBuilder.Entity<Block>().HasKey(x => x.BId);
            modelBuilder.Entity<Block>().Property(x => x.BId).HasColumnName(@"Id").IsRequired()
                .HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Block>().Property(y => y.BName).HasColumnName(@"Name").IsOptional()
                .HasColumnType("nvarchar").HasMaxLength(50);
            modelBuilder.Entity<Block>().Property(y => y.DistrictId).HasColumnName(@"DistrictId").IsRequired()
                .HasColumnType("int");

            modelBuilder.Entity<Block>().HasRequired(x => x.District)
                .WithMany(d => d.Blocks).HasForeignKey(t => t.DistrictId).WillCascadeOnDelete(false);
        }
    }
}
